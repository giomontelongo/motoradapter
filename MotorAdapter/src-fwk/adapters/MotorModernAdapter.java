package adapters;

import motorsNormal.MotorNormal;
import motorsModerno.MotorModern;

public class MotorModernAdapter implements MotorNormal {
	
	MotorModern MotorModern;
	
	public MotorModernAdapter(MotorModern MotorModern){
		this.MotorModern = MotorModern;
	}

	@Override
	public void TurnOn() {
		// TODO Auto-generated method stub
		MotorModern.Connect();
		MotorModern.Activate();
	}

	@Override
	public void SpeedUp() {
		// TODO Auto-generated method stub
		MotorModern.MoveFaster();
	}

	@Override
	public void TurnOff() {
		// TODO Auto-generated method stub
		MotorModern.Stop();
		MotorModern.Disconnect();
	}

}
