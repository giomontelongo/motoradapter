package motorsModerno;

public interface MotorModern {

	public void Connect();
	public void Activate();
	public void MoveFaster();
	public void Stop();
	public void Disconnect();
}
