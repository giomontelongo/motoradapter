package motorsModerno;

public class MotorElectric implements MotorModern {

	@Override
	public void Connect() {
		// TODO Auto-generated method stub
		System.out.println("The Motor Electric is connected.");
	}

	@Override
	public void Activate() {
		// TODO Auto-generated method stub
		System.out.println("The Motor Electric is activated.");
	}

	@Override
	public void MoveFaster() {
		// TODO Auto-generated method stub
		System.out.println("The Motor Electric moves faster.");
	}

	@Override
	public void Stop() {
		// TODO Auto-generated method stub
		System.out.println("The Motor Electric is stops.");
	}

	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub
		System.out.println("The Motor Electric is disconnected.");
	}

}
