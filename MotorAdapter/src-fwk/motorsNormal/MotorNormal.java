package motorsNormal;

public interface MotorNormal {
	
	public void TurnOn();
	public void SpeedUp();
	public void TurnOff();
	
}
