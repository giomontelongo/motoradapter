package app;

import java.util.*;
import adapters.MotorModernAdapter;
import motorsModerno.MotorElectric;
import motorsModerno.MotorModern;
import motorsNormal.MotorCommon;
import motorsNormal.MotorEconomic;
import motorsNormal.MotorNormal;

public class Simulator {
	
	public static void main(String[] args) {
		simulation();
	}
	
	public static void simulation(){
		
		Scanner read = new Scanner(System.in);
		String name;
		boolean out = false;
		int num, carryOn;
		
		MotorNormal motor;
	
		while(!out){
			System.out.println("Select a car:");
			name = read.next();
			System.out.println();
			System.out.println("Type of motor:");
			System.out.println("1) Motor Common.");
			System.out.println("2) Motor Economic.");
			System.out.println("3) Motor Electric.");
			System.out.println();
			num = read.nextInt();
			
			switch (num) {
				case 1:
					motor = new MotorCommon();
					simulate(motor, name);
					break;
				case 2:
					motor = new MotorEconomic();
					simulate(motor, name);
					break;
				case 3:
					MotorModern motorElectric = new MotorElectric();
					motor = new MotorModernAdapter(motorElectric);
					simulate(motor, name);
					break;
				default:
					System.out.println("Invalid number.");
					break;
			}
			System.out.println();
			System.out.println("Start the process again??");
			System.out.println("1) YES");
			System.out.println("2) NO");
			System.out.println();
			carryOn = read.nextInt();
			if(carryOn == 2){
				out = true;
			}
		}
		System.out.println("End of the process.");
	}
	
	public static void simulate(MotorNormal motor, String name){
		System.out.println("Car: "+name);
		motor.TurnOn();
		motor.SpeedUp();
		motor.TurnOff();
	}
}
